package com.yuejiekeji.photowall.controller;

import com.yuejiekeji.photowall.common.Const;
import com.yuejiekeji.photowall.common.Ret;
import com.yuejiekeji.photowall.entity.req.SignInReq;
import com.yuejiekeji.photowall.entity.req.SignUpReq;
import com.yuejiekeji.photowall.entity.res.SignInRes;
import com.yuejiekeji.photowall.service.UserService;
import com.yuejiekeji.photowall.util.HttpUtil;
import com.yuejiekeji.photowall.util.JWTUtil;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;


    @Resource
    private RedisTemplate redisTemplate;


    @RequestMapping(value = "/sign/up",method = RequestMethod.POST)
    public Ret signUp(HttpServletRequest request, SignUpReq req){
        req.setIp(HttpUtil.getIp(request));
        userService.signUp(req);
        return Ret.success();
    }


    @RequestMapping(value = "/sign/in",method = RequestMethod.POST)
    public Ret<SignInRes> signIn(HttpServletRequest request, SignInReq req){
        req.setIp(HttpUtil.getIp(request));
        SignInRes signInRes = userService.signIn(req);
        return Ret.success(signInRes);
    }

    @RequestMapping(value = "/logout",method = RequestMethod.POST)
    public Ret<Integer> logout(@RequestParam String token){

        Integer uid = JWTUtil.getUid(token);

        redisTemplate.delete(Const.RedisKeyPrefix.LOGIN_USER + uid);
        return Ret.success(uid);
    }


    //微信登录T

    @RequestMapping(value = "/sign/in/wx",method = RequestMethod.POST)
    public Ret<SignInRes> signInWx(HttpServletRequest request, SignInReq req){
        req.setIp(HttpUtil.getIp(request));
        SignInRes signInRes = userService.wxLogin(req.getCode(),req.getAvatar(),req.getNick(),req.getIp());
        return Ret.success(signInRes);
    }
    //支付宝登录

    //手机号一键登录
}
