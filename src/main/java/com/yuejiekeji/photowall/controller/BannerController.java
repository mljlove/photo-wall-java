package com.yuejiekeji.photowall.controller;

import com.yuejiekeji.photowall.common.Const;
import com.yuejiekeji.photowall.common.Ret;
import com.yuejiekeji.photowall.entity.Banner;
import com.yuejiekeji.photowall.entity.query.BannerQuery;
import com.yuejiekeji.photowall.entity.req.SignInReq;
import com.yuejiekeji.photowall.entity.req.SignUpReq;
import com.yuejiekeji.photowall.entity.res.BannerRes;
import com.yuejiekeji.photowall.entity.res.SignInRes;
import com.yuejiekeji.photowall.service.BannerService;
import com.yuejiekeji.photowall.service.UserService;
import com.yuejiekeji.photowall.util.HttpUtil;
import com.yuejiekeji.photowall.util.JWTUtil;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/banner")
public class BannerController {

    @Resource
    private BannerService bannerService;


    @RequestMapping(value = "/home",method = RequestMethod.GET)
    public Ret<List<BannerRes>> getHomeBanner(HttpServletRequest request){

        BannerQuery bannerQuery = new BannerQuery();
        bannerQuery.setLocation("home_banner");
        bannerQuery.setOrder("indx asc");

        List<BannerRes> bannerList = bannerService.bannerList(bannerQuery);


        return Ret.success(bannerList);
    }



}
