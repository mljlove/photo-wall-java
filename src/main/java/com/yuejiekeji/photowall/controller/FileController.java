package com.yuejiekeji.photowall.controller;

import com.yuejiekeji.photowall.common.Ret;
import com.yuejiekeji.photowall.entity.req.SignUpReq;
import com.yuejiekeji.photowall.service.ConfigService;
import com.yuejiekeji.photowall.service.UserService;
import com.yuejiekeji.photowall.util.HttpUtil;
import com.yuejiekeji.photowall.util.QiNiuUtil;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/file")
public class FileController {


    @Resource
    private ConfigService configService;


    @RequestMapping(value = "/getToken",method = RequestMethod.GET)
    public Ret<String> getToken(HttpServletRequest request){
        Map<String, String> qiniuConfig = configService.getMapByType("qiniu_config");

        String auth = QiNiuUtil.getAuth(qiniuConfig);
        return Ret.success(auth);
    }



}
