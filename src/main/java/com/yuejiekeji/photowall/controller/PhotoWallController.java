package com.yuejiekeji.photowall.controller;

import com.yuejiekeji.photowall.common.Ret;
import com.yuejiekeji.photowall.entity.query.BannerQuery;
import com.yuejiekeji.photowall.entity.query.PhotoWallQuery;
import com.yuejiekeji.photowall.entity.res.*;
import com.yuejiekeji.photowall.enums.EStatus;
import com.yuejiekeji.photowall.service.BannerService;
import com.yuejiekeji.photowall.service.PhotoWallService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/wall")
public class PhotoWallController {

    @Resource
    private PhotoWallService photoWallService;


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public Ret<List<HomeWallRes>> getHomeList(HttpServletRequest request) {

        PhotoWallQuery query = new PhotoWallQuery();
        query.setStatus(EStatus.NORMAL.getCode());
        query.setOrder("indx asc");
        List<HomeWallRes> resList = photoWallService.getHomeWallList(query);
        return Ret.success(resList);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Ret add(HttpServletRequest request, @RequestBody String json) {
        photoWallService.saveBgCategory(json);
        return Ret.success();
    }

    @RequestMapping(value = "/bg", method = RequestMethod.GET)
    public Ret<List<BgCategoryRes>> getBgCategory(HttpServletRequest request) {
        List<BgCategoryRes> resList = photoWallService.getBgCategory();
        return Ret.success(resList);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Ret<UserPhotoWallRes> createPhotoWal(HttpServletRequest request, @RequestParam Integer uid,
                                                @RequestParam Long wallId,
                                                @RequestParam String bgImg) {

        UserPhotoWallRes res = photoWallService.createPhotoWall(uid, wallId, bgImg);
        return Ret.success(res);
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public Ret<PhotoWallDetailRes> getPhotoWalDetail(HttpServletRequest request, @RequestParam Integer uid,
                                                     @RequestParam Long id) {

        PhotoWallDetailRes res = photoWallService.getPhotoDetail(id, uid);
        return Ret.success(res);
    }


    @RequestMapping(value = "/addImage", method = RequestMethod.POST)
    public Ret addImage(HttpServletRequest request, @RequestParam Integer uid,
                        @RequestParam Integer indx, @RequestParam String img, @RequestParam Long itemId) {

        photoWallService.addImage(itemId, uid, indx, img);
        return Ret.success();
    }


}
