package com.yuejiekeji.photowall.util;

import com.qiniu.util.Auth;

import java.util.Map;

public class QiNiuUtil {


    public static String getAuth(Map<String,String> config){

       String sk =  config.get("sk");
       String ak =  config.get("ak");
       String bucketName =  config.get("bucket");

        Auth auth = Auth.create(ak,sk);
        return auth.uploadToken(bucketName,null,300L,null);
    }


    public static void main(String[] args) {
    }

}
