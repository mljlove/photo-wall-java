package com.yuejiekeji.photowall.common;

import com.yuejiekeji.photowall.enums.EResponseCode;

public class Ret<T> {
    private Integer code;

    private String enCode;

    private String msg;

    private T data;

    public static <T> Ret<T> success(T data){
       return new Ret().code(EResponseCode.SUCCESS.getCode()).enCode(EResponseCode.SUCCESS.getEnCode())
                .msg(EResponseCode.SUCCESS.getMsg()).data(data);
    }

    public static Ret success(){
        return success(new Object());
    }

    public static Ret error(){
        return error(EResponseCode.FAIL.getCode());
    }

    public static Ret error(Integer code){
        EResponseCode responseCode = EResponseCode.getByCode(code);
        return error(code,responseCode.getMsg());
    }


    public static Ret error(Integer code,String msg){
        EResponseCode responseCode = EResponseCode.getByCode(code);
        return new Ret().code(responseCode.getCode()).enCode(responseCode.getEnCode())
                .msg(msg);
    }


    public Ret code(Integer code){
        this.code = code;
        return this;
    }

    public Ret enCode(String enCode){
        this.enCode = enCode;
        return this;
    }

    public Ret msg(String msg){
        this.msg = msg;
        return this;
    }

    public Ret data(T data){
        this.data = data;
        return this;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setEnCode(String enCode) {
        this.enCode = enCode;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public String getEnCode() {
        return enCode;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }
}
