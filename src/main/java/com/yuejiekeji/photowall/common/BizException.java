package com.yuejiekeji.photowall.common;

import com.yuejiekeji.photowall.enums.EResponseCode;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class BizException extends RuntimeException {

    private static final Object[] EMPTY_PARAMS = new Object[0];

    private Integer errorCode = 200;


    /**
     * 错误参数清单
     */
    private Object[] errorParams;

    /**
     * 按照put的顺序保存错误参数
     */
    private List<String> errorPropNames = new ArrayList<String>();

    private Map<String, Object> errorProperties = new HashMap<String, Object>();

    public BizException(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public BizException(Integer errorCode, Object... errorParams) {
        this.errorCode = errorCode;
        this.errorParams = errorParams;
    }

    public BizException(Throwable cause, Integer errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public BizException(Throwable cause, Integer errorCode,
                        Object[] errorParams) {
        super(cause);
        this.errorCode = errorCode;
        this.errorParams = errorParams;
    }

    public BizException put(String name, Object prop) {
        if (name != null) {
            errorPropNames.add(name);
            errorProperties.put(name, prop);
        }
        return this;
    }



    public Integer getErrorCode() {
        return errorCode;
    }

    public Object[] getErrorParams() {
        return errorParams;
    }

    public List<String> getErrorPropNames() {
        return errorPropNames;
    }

    public Map<String, Object> getErrorProperties() {
        return errorProperties;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorParams(Object[] errorParams) {
        this.errorParams = errorParams;
    }

    public void setErrorPropNames(List<String> errorPropNames) {
        this.errorPropNames = errorPropNames;
    }

    public void setErrorProperties(Map<String, Object> errorProperties) {
        this.errorProperties = errorProperties;
    }


    @Override
    public String getMessage() {
        String message = EResponseCode.getByCode(getErrorCode()).getMsg();
        return MessageFormat.format(message,getErrorParams());
    }


}
