package com.yuejiekeji.photowall.entity.req;

import lombok.Data;

@Data
public class BaseReq {

    /**
     * 注册IP
     */
    private String ip;
}
