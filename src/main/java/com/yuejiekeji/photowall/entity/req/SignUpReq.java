package com.yuejiekeji.photowall.entity.req;

import lombok.Data;

@Data
public class SignUpReq extends BaseReq{

    /**
     * 账号
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;


}
