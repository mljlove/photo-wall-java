package com.yuejiekeji.photowall.entity.req;

import lombok.Data;

@Data
public class SignInReq extends BaseReq{

    /**
     * 账号
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;


    private String code;
    private String avatar;
    private String nick;
    private String ip;


}
