package com.yuejiekeji.photowall.entity.query;

import lombok.Data;


@Data
public class BgCategoryQuery {
    private Integer id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类图片
     */
    private String img;

    /**
     * 排序
     */
    private Integer indx;

    /**
     * 状态1 正常2 禁用
     */
    private Byte status;

    private String order;

    private Integer size;


}