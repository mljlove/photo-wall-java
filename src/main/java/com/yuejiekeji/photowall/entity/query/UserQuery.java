package com.yuejiekeji.photowall.entity.query;

import lombok.Data;

@Data
public class UserQuery {
    private Integer id;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 昵称
     */
    private String nick;

    /**
     * 密码
     */
    private String pwd;

    private String openUid;
}
