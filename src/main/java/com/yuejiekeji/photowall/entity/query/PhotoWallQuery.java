package com.yuejiekeji.photowall.entity.query;

import lombok.Data;


@Data
public class PhotoWallQuery {
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 标题
     */
    private String title;

    /**
     * 背景图
     */
    private String bgImg;

    /**
     * 排序
     */
    private Integer indx;

    /**
     * 坐标
     */
    private String coordinate;

    /**
     * 状态 1 正常 2 下架
     */
    private Byte status;

    /**
     * 标志  0 默认  1 新增 2 推荐
     */
    private Byte flag;

    private String order;

    private Integer size;


}