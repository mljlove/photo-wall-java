package com.yuejiekeji.photowall.entity.query;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class BannerQuery {
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 标题
     */
    private String title;

    /**
     * 目标跳转地址
     */
    private String targetUrl;

    /**
     * 图片
     */
    private String image;

    /**
     * 访问类型  1 外联  2 APP内部
     */
    private Byte type;

    /**
     * 位置
     */
    private String location;
    private String order;
    private Integer size;


}