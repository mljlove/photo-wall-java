package com.yuejiekeji.photowall.entity.query;

import lombok.Data;


@Data
public class ConfigQuery {
    private Integer id;

    private String type;

    private String key;

    private String value;

    private String category;

    private String order;
    private Integer size;


}