package com.yuejiekeji.photowall.entity.query;

import lombok.Data;


@Data
public class BgCategoryImageQuery {
    private Integer id;

    /**
     * 背景图ID
     */
    private Integer bgId;

    /**
     * 背景图分类ID
     */
    private Integer cId;

    private String order;
    private Integer size;


}