package com.yuejiekeji.photowall.entity.query;

import lombok.Data;


@Data
public class BgImageQuery {
    private Integer id;

    private String img;

    private Integer indx;

    private Byte status;

    private Integer cid;

    private String order;

    private Integer size;


}