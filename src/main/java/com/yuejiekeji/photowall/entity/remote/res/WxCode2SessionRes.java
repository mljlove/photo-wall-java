package com.yuejiekeji.photowall.entity.remote.res;

import lombok.Data;

@Data
public class WxCode2SessionRes {
    private String session_key;
    private String unionid;
    private String errmsg;
    private String openid;
    private String errcode;
}
