package com.yuejiekeji.photowall.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 * 用户照片墙 照片库
 */
public class UserWallItemPhoto implements Serializable {
    private Long id;

    /**
     * 条目ID
     */
    private Long itemId;

    /**
     * 上传人昵称
     */
    private String nick;

    /**
     * 上传人头像
     */
    private String photo;

    /**
     * 图片地址
     */
    private String img;

    /**
     * 上传人微信ID
     */
    private String wxId;

    /**
     * 是否是本人 1 是  2 否
     */
    private Byte owner;

    /**
     * 状态 1 正常 2 下架 3 删除
     */
    private Byte status;

    /**
     * 照片位置索引
     */
    private Integer indx;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }

    public Byte getOwner() {
        return owner;
    }

    public void setOwner(Byte owner) {
        this.owner = owner;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Integer getIndx() {
        return indx;
    }

    public void setIndx(Integer indx) {
        this.indx = indx;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}