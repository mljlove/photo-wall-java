package com.yuejiekeji.photowall.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 * 首页展示分类
 */
public class HomeCategory implements Serializable {
    private Integer id;

    /**
     * 展示名称
     */
    private String name;

    /**
     * 图标
     */
    private String icon;

    /**
     * 三方名字
     */
    private String refName;

    /**
     * 三方ID
     */
    private Integer refId;

    /**
     * 渠道类型  jd tb pdd
     */
    private String channelType;

    /**
     * 状态 1 上架 2 下架
     */
    private Byte status;

    /**
     * 排序
     */
    private Integer indx;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Integer getIndx() {
        return indx;
    }

    public void setIndx(Integer indx) {
        this.indx = indx;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}