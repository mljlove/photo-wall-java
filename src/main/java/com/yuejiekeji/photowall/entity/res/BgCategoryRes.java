package com.yuejiekeji.photowall.entity.res;

import com.yuejiekeji.photowall.entity.BgImage;
import lombok.Data;

import java.util.List;

@Data
public class BgCategoryRes {

    private String img;

    private Integer id;

    private String name;

    private List<BgImageRes> bgImageList;

}
