package com.yuejiekeji.photowall.entity.res;

import lombok.Data;

import java.util.List;

@Data
public class PhotoWallDetailRes {

    private Long id;

    /**
     * 背景图
     */
    private String bgImg;

    /**
     * 坐标
     */
    private String coordinate;

    private List<PhotoWallImageRes> images;


}
