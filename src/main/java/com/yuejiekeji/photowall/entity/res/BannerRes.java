package com.yuejiekeji.photowall.entity.res;

import lombok.Data;

@Data
public class BannerRes {

    private String img;

    private String url;
}
