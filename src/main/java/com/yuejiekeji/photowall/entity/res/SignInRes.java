package com.yuejiekeji.photowall.entity.res;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignInRes {

    private Integer id;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 昵称
     */
    private String nick;

    /**
     * 头像
     */
    private String avatar;


    private String token;
}
