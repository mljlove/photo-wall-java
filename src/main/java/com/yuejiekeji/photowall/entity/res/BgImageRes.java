package com.yuejiekeji.photowall.entity.res;

import lombok.Data;

@Data
public class BgImageRes {

    private String img;

    private Integer id;

    private String name;


}
