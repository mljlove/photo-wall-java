package com.yuejiekeji.photowall.entity.res;

import lombok.Data;

import java.util.List;

@Data
public class PhotoWallImageRes {

    /**
     * 条目ID
     */
    private Long id;

    /**
     * 上传人昵称
     */
    private String nick;

    /**
     * 上传人头像
     */
    private String photo;

    /**
     * 图片地址
     */
    private String img;

    /**
     * 上传人微信ID
     */
    private String wxId;

    /**
     * 是否是本人 1 是  2 否
     */
    private Byte owner;


    /**
     * 照片位置索引
     */
    private Integer indx;


}
