package com.yuejiekeji.photowall.entity.res;

import lombok.Data;

@Data
public class HomeWallRes {

    private String img;

    private Long id;

    private String name;
    /**
     * 页面跳转地址
     */
    private String pageUrl;

    /**
     * 背景颜色
     */
    private String bgColor;
}
