package com.yuejiekeji.photowall.enums;

/**
 * 标准状态 枚举
 */
public enum EBoolean {

    NO((byte) 0,"否"),
    YES((byte) 1,"是")
    ;

    private byte code;
    private String value;

    EBoolean(Byte code, String value) {
        this.code = code;
        this.value = value;
    }

    public Byte getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
