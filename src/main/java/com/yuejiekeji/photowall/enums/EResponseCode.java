package com.yuejiekeji.photowall.enums;

public enum EResponseCode {

    //请求失败
    FAIL(500,"请求失败","fail"),

    ACCOUNT_ERROR(401,"账号或密码错误","account error"),
    TOKEN_ERROR(402,"登录信息已过期,请重新登录","token error"),

    UNKNOWN(9999,"未知异常","unknown error"),
    SUCCESS(200,"成功","success")
    ;

    private Integer code;
    private String msg;
    private String enCode;

    public static EResponseCode getByCode(Integer code){

        for (EResponseCode responseCode : EResponseCode.values()){
            if (responseCode.code.equals(code)){
                return responseCode;
            }
        }
        return EResponseCode.UNKNOWN;
    }

    EResponseCode(Integer code, String msg, String enCode) {
        this.code = code;
        this.msg = msg;
        this.enCode = enCode;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public String getEnCode() {
        return enCode;
    }
}
