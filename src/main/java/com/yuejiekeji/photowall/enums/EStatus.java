package com.yuejiekeji.photowall.enums;

/**
 * 标准状态 枚举
 */
public enum EStatus {

    DELETE((byte) 3,"下架"),
    DISABLE((byte) 2,"禁用"),
    NORMAL((byte) 1,"正常"),
    INIT((byte) 0,"初始化")
    ;

    private byte code;
    private String value;

    EStatus(Byte code, String value) {
        this.code = code;
        this.value = value;
    }

    public Byte getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
