package com.yuejiekeji.photowall.enums;

public enum ERegisterType {

    PHONE((byte) 2,"禁用"),
    WECHAT((byte) 1,"微信")
    ;

    private byte code;
    private String value;

    ERegisterType(Byte code, String value) {
        this.code = code;
        this.value = value;
    }

    public Byte getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
