package com.yuejiekeji.photowall.service.impl;

import com.yuejiekeji.photowall.entity.Banner;
import com.yuejiekeji.photowall.entity.query.BannerQuery;
import com.yuejiekeji.photowall.entity.res.BannerRes;
import com.yuejiekeji.photowall.mapper.BannerMapper;
import com.yuejiekeji.photowall.service.BannerService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BannerServiceImpl implements BannerService {

    @Resource
    private BannerMapper bannerMapper;



    @Override
    public List<BannerRes> bannerList(BannerQuery query){

        List<Banner> banners = bannerMapper.selectByCondition(query);
        List<BannerRes> resList = new ArrayList<>();
        for (Banner banner:banners){
            BannerRes  bannerRes = new BannerRes();
            bannerRes.setImg(banner.getImage());
            bannerRes.setUrl(banner.getTargetUrl());
            resList.add(bannerRes);

        }
       return resList;
    }
}
