package com.yuejiekeji.photowall.service.impl;

import cn.hutool.core.util.StrUtil;
import com.yuejiekeji.photowall.entity.Config;
import com.yuejiekeji.photowall.entity.query.ConfigQuery;
import com.yuejiekeji.photowall.mapper.ConfigMapper;
import com.yuejiekeji.photowall.service.ConfigService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConfigServiceImpl implements ConfigService {

    @Resource
    private ConfigMapper configMapper;

    @Override
    public Map<String, String> getMapByType(String type){

        Map<String,String> resMap = new HashMap<>();

        if (StrUtil.isBlank(type)){
            return resMap;
        }

        ConfigQuery configQuery = new ConfigQuery();
        configQuery.setType(type);

        List<Config> configList = configMapper.selectByCondition(configQuery);
        for(Config config:configList){
            resMap.put(config.getKey(),config.getValue());
        }
        return resMap;
    }











}
