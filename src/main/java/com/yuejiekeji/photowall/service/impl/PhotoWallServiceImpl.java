package com.yuejiekeji.photowall.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yuejiekeji.photowall.entity.*;
import com.yuejiekeji.photowall.entity.query.BgCategoryQuery;
import com.yuejiekeji.photowall.entity.query.BgImageQuery;
import com.yuejiekeji.photowall.entity.query.PhotoWallQuery;
import com.yuejiekeji.photowall.entity.res.*;
import com.yuejiekeji.photowall.enums.EBoolean;
import com.yuejiekeji.photowall.enums.EStatus;
import com.yuejiekeji.photowall.mapper.*;
import com.yuejiekeji.photowall.service.PhotoWallService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PhotoWallServiceImpl implements PhotoWallService {

    @Resource
    private PhotoWallMapper photoWallMapper;

    @Resource
    private BgImageMapper bgImageMapper;

    @Resource
    private BgCategoryImageMapper bgCategoryImageMapper;

    @Resource
    private BgCategoryMapper bgCategoryMapper;

    @Resource
    private UserPhotoWallItemMapper userPhotoWallItemMapper;


    @Resource
    private UserWallItemPhotoMapper userWallItemPhotoMapper;


    @Resource
    private UserMapper userMapper;




    @Override
    public List<HomeWallRes> getHomeWallList(PhotoWallQuery query){

        List<PhotoWall> photoWalls = photoWallMapper.selectByCondition(query);

        List<HomeWallRes> resList = new ArrayList<>();
        for (PhotoWall photoWall:photoWalls){
            HomeWallRes res = new HomeWallRes();
            res.setId(photoWall.getId());
            res.setImg(photoWall.getBgImg());
            res.setName(photoWall.getName());
            res.setPageUrl(photoWall.getPageUrl());
            res.setBgColor(photoWall.getBgColor());
            resList.add(res);
        }
        return resList;
    }




    public String subString(String themeBgUrl){
        int indx = StrUtil.indexOf(themeBgUrl, '/', 8);

       return StrUtil.sub(themeBgUrl,indx,themeBgUrl.length());
    }



    @Override
    public void saveBgCategory(String json){
        JSONObject object = JSON.parseObject(json);

        JSONObject data =  object.getJSONObject("data");

        String host = "http://static.test.com";

        JSONArray themeSets = data.getJSONArray("themeSets");


        for (int i = 0;i<themeSets.size()-1;i++){
            JSONObject theme =  themeSets.getJSONObject(i);


            String groupName =  theme.getString("groupName");

            BgCategory bgCategory = new BgCategory();
            bgCategory.setName(groupName);
            bgCategory.setIndx(99);
            bgCategory.setStatus(EStatus.NORMAL.getCode());
            bgCategoryMapper.insertSelective(bgCategory);


            JSONArray themeChildSets = theme.getJSONArray("themeSets");

            for (int j = 0;j<themeChildSets.size();j++){

                JSONObject themeChild =  themeChildSets.getJSONObject(j);

                String bgName =  themeChild.getString("themeName");
                String titleUrl =  themeChild.getString("titleUrl").replace("\\","");
                String themeBgUrl =  themeChild.getString("themeBgUrl").replace("\\","");

                BgImage bgImage = new BgImage();
                bgImage.setName(URLDecoder.decode(bgName, StandardCharsets.UTF_8));
                bgImage.setImg(host + subString(themeBgUrl));
                bgImage.setThemeImg(host + subString(titleUrl));
                bgImage.setIndx(99);
                bgImage.setStatus(EStatus.NORMAL.getCode());
                bgImageMapper.insertSelective(bgImage);


                BgCategoryImage bgCategoryImage = new BgCategoryImage();
                bgCategoryImage.setBgId(bgImage.getId());
                bgCategoryImage.setcId(bgCategory.getId());
                bgCategoryImageMapper.insertSelective(bgCategoryImage);

            }

        }





    }


    @Override
    public List<BgCategoryRes> getBgCategory(){

        BgCategoryQuery bgCategoryQuery = new BgCategoryQuery();
        bgCategoryQuery.setStatus(EStatus.NORMAL.getCode());
        bgCategoryQuery.setOrder("indx ASC");
        List<BgCategoryRes> resList = new ArrayList<>();

        List<BgCategory> bgCategories = bgCategoryMapper.selectByCondition(bgCategoryQuery);
        for (BgCategory bgCategory:bgCategories){
            BgCategoryRes res = new BgCategoryRes();
            res.setName(bgCategory.getName());
            res.setId(bgCategory.getId());
            BgImageQuery bgImageQuery = new BgImageQuery();
            bgImageQuery.setStatus(EStatus.NORMAL.getCode());
            bgImageQuery.setCid(bgCategory.getId());
            bgImageQuery.setOrder("indx ASC");
            List<BgImage> bgImageList = bgImageMapper.selectByCondition(bgImageQuery);
            List<BgImageRes> bgImageResList = new ArrayList<>();
            for (BgImage bgImage:bgImageList){
                BgImageRes bgImageRes = new BgImageRes();
                bgImageRes.setId(bgImage.getId());
                bgImageRes.setName(bgImage.getName());
                bgImageRes.setImg(bgImage.getImg());
                bgImageResList.add(bgImageRes);
            }

            res.setBgImageList(bgImageResList);
            resList.add(res);

        }


        return resList;

    }


    @Override
    public UserPhotoWallRes createPhotoWall(Integer uid, Long wallId, String bgImg){

        PhotoWall photoWall = photoWallMapper.selectByPrimaryKey(wallId);
        if (Objects.isNull(photoWall)){
            return null;
        }

        UserPhotoWallItem userPhotoWallItem = new UserPhotoWallItem();
        userPhotoWallItem.setUid(uid);
        userPhotoWallItem.setWallId(wallId);
        userPhotoWallItem.setBgImg(bgImg);
        userPhotoWallItem.setCoordinate(photoWall.getCoordinate());
        long currTime = System.currentTimeMillis();
        userPhotoWallItem.setStartTime(currTime);
        userPhotoWallItem.setEndTime(currTime + 30*86400000L);
        userPhotoWallItemMapper.insertSelective(userPhotoWallItem);
        UserPhotoWallRes res = new UserPhotoWallRes();
        res.setId(userPhotoWallItem.getId());

        return res;

    }


    /**
     * 获取照片墙详情
     * @param id id
     * @param uid 用户ID
     * @return PhotoWallDetailRes
     */
    @Override
    public PhotoWallDetailRes getPhotoDetail(Long id,Integer uid){

        UserPhotoWallItem userPhotoWallItem = userPhotoWallItemMapper.selectByPrimaryKey(id);

        if (!Objects.equals(uid,userPhotoWallItem.getUid())){
            return null;
        }

        List<UserWallItemPhoto> userWallItemPhotoList = userWallItemPhotoMapper.selectByItemId(userPhotoWallItem.getId());

        String prefix = "http://sp1f42c80.hd-bkt.clouddn.com/";
        List<PhotoWallImageRes> imageResList = new ArrayList<>();
        for (UserWallItemPhoto userWallItemPhoto:userWallItemPhotoList){
            PhotoWallImageRes res = new PhotoWallImageRes();

            BeanUtil.copyProperties(userWallItemPhoto,res);

            res.setImg(prefix + userWallItemPhoto.getImg());
            imageResList.add(res);
        }
        PhotoWallDetailRes res = new PhotoWallDetailRes();
        res.setCoordinate(userPhotoWallItem.getCoordinate());
        res.setImages(imageResList);
        res.setBgImg(userPhotoWallItem.getBgImg());
        res.setId(userPhotoWallItem.getId());


        return res;

    }


    @Override
    public void addImage(Long itemId,Integer uid,Integer indx,String img){

        UserPhotoWallItem userPhotoWallItem = userPhotoWallItemMapper.selectByPrimaryKey(itemId);
        if (Objects.isNull(userPhotoWallItem)){
            return;
        }
        boolean isOwner = !Objects.equals(uid,userPhotoWallItem.getUid());

        User user = userMapper.selectByPrimaryKey(uid);

        UserWallItemPhoto userWallItemPhoto = new UserWallItemPhoto();
        userWallItemPhoto.setItemId(itemId);
        userWallItemPhoto.setNick(user.getNick());
        userWallItemPhoto.setPhoto(user.getAvatar());
        userWallItemPhoto.setImg(img);
        userWallItemPhoto.setWxId(uid+"");
        userWallItemPhoto.setOwner(isOwner? EBoolean.YES.getCode():EBoolean.NO.getCode());
        userWallItemPhoto.setStatus(EStatus.NORMAL.getCode());
        userWallItemPhoto.setIndx(indx);


        userWallItemPhotoMapper.insertSelective(userWallItemPhoto);

    }



}
