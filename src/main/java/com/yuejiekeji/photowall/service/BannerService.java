package com.yuejiekeji.photowall.service;

import com.yuejiekeji.photowall.entity.Banner;
import com.yuejiekeji.photowall.entity.query.BannerQuery;
import com.yuejiekeji.photowall.entity.res.BannerRes;

import java.util.List;

public interface BannerService {
    List<BannerRes> bannerList(BannerQuery query);
}
