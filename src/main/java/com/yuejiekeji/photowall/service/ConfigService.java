package com.yuejiekeji.photowall.service;

import java.util.Map;

public interface ConfigService {

    Map<String, String> getMapByType(String type);
}
