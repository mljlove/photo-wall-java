package com.yuejiekeji.photowall.service;

import com.yuejiekeji.photowall.entity.query.PhotoWallQuery;
import com.yuejiekeji.photowall.entity.res.BgCategoryRes;
import com.yuejiekeji.photowall.entity.res.HomeWallRes;
import com.yuejiekeji.photowall.entity.res.PhotoWallDetailRes;
import com.yuejiekeji.photowall.entity.res.UserPhotoWallRes;

import java.util.List;

public interface PhotoWallService {
    List<HomeWallRes> getHomeWallList(PhotoWallQuery query);

    void saveBgCategory(String json);

    List<BgCategoryRes> getBgCategory();

    UserPhotoWallRes createPhotoWall(Integer uid, Long wallId, String bgImg);

    PhotoWallDetailRes getPhotoDetail(Long id, Integer uid);

    void addImage(Long itemId, Integer uid, Integer indx, String img);
}
