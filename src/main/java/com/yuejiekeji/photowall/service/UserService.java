package com.yuejiekeji.photowall.service;

import com.yuejiekeji.photowall.entity.req.SignInReq;
import com.yuejiekeji.photowall.entity.req.SignUpReq;
import com.yuejiekeji.photowall.entity.res.SignInRes;

public interface UserService {
    void signUp(SignUpReq req);

    SignInRes signIn(SignInReq req);

    SignInRes wxLogin(String code, String avatar, String nick, String ip);
}
