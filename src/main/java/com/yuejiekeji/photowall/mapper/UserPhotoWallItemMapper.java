package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.UserPhotoWallItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserPhotoWallItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserPhotoWallItem record);

    int insertSelective(UserPhotoWallItem record);

    UserPhotoWallItem selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserPhotoWallItem record);

    int updateByPrimaryKey(UserPhotoWallItem record);
}