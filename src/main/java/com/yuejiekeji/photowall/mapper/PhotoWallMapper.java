package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.PhotoWall;
import com.yuejiekeji.photowall.entity.query.PhotoWallQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PhotoWallMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PhotoWall record);

    int insertSelective(PhotoWall record);

    PhotoWall selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PhotoWall record);

    int updateByPrimaryKey(PhotoWall record);

    List<PhotoWall> selectByCondition(PhotoWallQuery record);
}