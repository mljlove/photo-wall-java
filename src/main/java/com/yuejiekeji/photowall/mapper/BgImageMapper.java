package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.BgImage;
import com.yuejiekeji.photowall.entity.query.BgImageQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BgImageMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BgImage record);

    int insertSelective(BgImage record);

    BgImage selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BgImage record);

    int updateByPrimaryKey(BgImage record);

    List<BgImage> selectByCondition(BgImageQuery record);
}