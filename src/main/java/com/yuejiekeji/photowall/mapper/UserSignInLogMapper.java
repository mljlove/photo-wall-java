package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.UserSignInLog;

public interface UserSignInLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserSignInLog record);

    int insertSelective(UserSignInLog record);

    UserSignInLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserSignInLog record);

    int updateByPrimaryKey(UserSignInLog record);
}