package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.UserWallItemPhoto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserWallItemPhotoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserWallItemPhoto record);

    int insertSelective(UserWallItemPhoto record);

    UserWallItemPhoto selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserWallItemPhoto record);

    int updateByPrimaryKey(UserWallItemPhoto record);
    List<UserWallItemPhoto> selectByItemId(@Param("itemId") Long itemId);
}