package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
//    User checkPassword(@Param("loginName") String loginName,@Param("pwd") String pwd);



    User getByUserName(@Param("username")String username);
}