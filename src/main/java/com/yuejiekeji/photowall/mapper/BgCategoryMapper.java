package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.BgCategory;
import com.yuejiekeji.photowall.entity.BgImage;
import com.yuejiekeji.photowall.entity.query.BgCategoryQuery;
import com.yuejiekeji.photowall.entity.query.BgImageQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BgCategoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BgCategory record);

    int insertSelective(BgCategory record);

    BgCategory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BgCategory record);

    int updateByPrimaryKey(BgCategory record);

    List<BgCategory> selectByCondition(BgCategoryQuery record);
}