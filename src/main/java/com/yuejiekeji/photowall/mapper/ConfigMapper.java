package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.Config;
import com.yuejiekeji.photowall.entity.query.ConfigQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Config record);

    int insertSelective(Config record);

    Config selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Config record);

    int updateByPrimaryKey(Config record);
    List<Config> selectByCondition(ConfigQuery record);
}