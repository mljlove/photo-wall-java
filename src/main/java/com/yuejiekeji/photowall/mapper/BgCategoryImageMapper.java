package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.BgCategory;
import com.yuejiekeji.photowall.entity.BgCategoryImage;
import com.yuejiekeji.photowall.entity.query.BgCategoryImageQuery;
import com.yuejiekeji.photowall.entity.query.BgCategoryQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BgCategoryImageMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BgCategoryImage record);

    int insertSelective(BgCategoryImage record);

    BgCategoryImage selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BgCategoryImage record);

    int updateByPrimaryKey(BgCategoryImage record);

    List<BgCategoryImage> selectByCondition(BgCategoryImageQuery record);
}