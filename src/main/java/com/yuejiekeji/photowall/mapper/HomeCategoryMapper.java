package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.HomeCategory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HomeCategoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HomeCategory record);

    int insertSelective(HomeCategory record);

    HomeCategory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HomeCategory record);

    int updateByPrimaryKey(HomeCategory record);
}