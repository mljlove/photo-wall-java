package com.yuejiekeji.photowall.mapper;

import com.yuejiekeji.photowall.entity.Banner;
import com.yuejiekeji.photowall.entity.query.BannerQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BannerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Banner record);

    int insertSelective(Banner record);

    Banner selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Banner record);

    int updateByPrimaryKey(Banner record);
    List<Banner> selectByCondition(BannerQuery record);
}